package com.brom.exceptions;

public class FPressedException extends Exception {
    public FPressedException(String message) {
        super(message);
    }
}
