package com.brom.exceptions;

public class TokenHolder implements AutoCloseable {
    private Token token;
    public TokenHolder(Token token) {
        if (token.chooseOwner(this).isPresent()) {
            this.token = token;
        }

    }
    private void freeToken() throws WrongTokenHolderException {
        this.token.letTheDobbyGo(this);
    }
    @Override
    public void close() throws Exception {
        this.freeToken();
        throw new DobbyIsFreeException();
    }
}
