package com.brom.exceptions;

public class QPressedException extends Exception {
    public QPressedException(String message) {
        super(message);
    }
}
