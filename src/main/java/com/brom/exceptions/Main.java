package com.brom.exceptions;

public class Main {
    public static void main(String[] args) throws FPressedException {
        try (TokenHolder tokenHolder = new TokenHolder(Token.getInstance())) {
            System.out.println("Something happened here");
        } catch (DobbyIsFreeException ex) {
            throw new FPressedException("+rep");
        } catch (Exception ex) {
            System.out.println("idk");
        }
    }
}
