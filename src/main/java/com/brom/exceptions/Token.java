package com.brom.exceptions;

import java.util.Optional;

public class Token {
    private static final Token token = new Token();
    private boolean held = false;
    private TokenHolder tokenHolder;

    public static Token getInstance() {
        return token;
    }

    public Optional<Token> chooseOwner(TokenHolder tokenHolder) {
        if (!held) {
            this.tokenHolder = tokenHolder;
            this.held = true;
            return Optional.of(token);
        } else {
            return Optional.empty();
        }
    }

    public void letTheDobbyGo(TokenHolder tokenHolder) throws WrongTokenHolderException {
        if (tokenHolder != this.tokenHolder) {
            throw new WrongTokenHolderException();
        } else {
            this.held = false;
            this.tokenHolder = null;
        }


    }

}
