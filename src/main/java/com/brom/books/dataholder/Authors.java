package com.brom.books.dataholder;

import com.brom.books.model.Author;

import java.util.ArrayList;
import java.util.List;

public class Authors {
    private static Authors instance = null;
    private static List<Author> authors;
    private Authors() {}
    public static Authors getInstance() {
        if (instance == null) {
            instance = new Authors();
        }
        return instance;
    }
    public static void initialize() {
        authors = new ArrayList<>();
        authors.add(
                new Author(1, "Christopher", "Paolini")
        );
        authors.add(
                new Author(2, "Herbert", "Shieldt")
        );
    }
    public Author getById(long id) {
        return authors.stream().filter(author -> author.getId() == id).findFirst().orElse(null);
    }
}
