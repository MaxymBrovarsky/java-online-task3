package com.brom.books.dataholder;

import com.brom.books.model.Author;
import com.brom.books.model.PublishingHouse;

import java.util.ArrayList;
import java.util.List;

public class PublishingHouses {
    private static PublishingHouses instance = null;
    private static List<PublishingHouse> publishingHouses;
    private PublishingHouses() {}
    public static PublishingHouses getInstance() {
        if (instance == null) {
            instance = new PublishingHouses();
        }
        return instance;
    }
    public static void initialize() {
        publishingHouses = new ArrayList<>();
        publishingHouses.add(
                new PublishingHouse(1, "CustomName", "Lviv")
        );
        publishingHouses.add(
                new PublishingHouse(2, "Absolute", "Lviv")
        );
    }
    public PublishingHouse getById(long id) {
        return publishingHouses.stream().filter(publishingHouse -> {
            return publishingHouse.getId() == id;
        }).findFirst().orElse(null);
    }

}
