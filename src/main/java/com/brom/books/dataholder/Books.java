package com.brom.books.dataholder;

import com.brom.books.model.Author;
import com.brom.books.model.BindingType;
import com.brom.books.model.Book;
import com.brom.books.model.PublishingHouse;
import java.util.ArrayList;
import java.util.List;

public class Books {
    private static Books instance = null;
    private static List<Book> books;
    private Books() {}
    public static Books getInstance() {
        if (instance == null) {
            instance = new Books();
        }
        return instance;
    }
    public static void initialize() {
        books = new ArrayList<>();
        PublishingHouses publishingHouses = PublishingHouses.getInstance();
        Authors authors = Authors.getInstance();
        List<Author> firstBookAuthors = new ArrayList<>();
        firstBookAuthors.add(authors.getById(1));
        List<Author> secondBookAuthors = new ArrayList<>();
        secondBookAuthors.add(authors.getById(2));
        books.add(
                new Book(
                        1,
                        "Eragon",
                        firstBookAuthors,
                        publishingHouses.getById(1),
                        2002,
                        800,
                        200.0,
                        BindingType.HARD
                )
        );
        books.add(
                new Book(
                        2,
                        "Java complete reference",
                        secondBookAuthors,
                        publishingHouses.getById(2),
                        2002,
                        800,
                        200.0,
                        BindingType.HARD
                )
        );
    }

    public List<Book> getByAuthor(long id) {
        Authors authors = Authors.getInstance();
        Author author = authors.getById(id);
        List<Book> booksByAuthor = new ArrayList<>();
        for (Book book: books) {
            if (book.getAuthors().contains(author)) {
                booksByAuthor.add(book);
            }
        }
        return booksByAuthor;
    }
    public List<Book> getByPublishingHouse(long id) {
        PublishingHouses publishingHouses = PublishingHouses.getInstance();
        PublishingHouse publishingHouse = publishingHouses.getById(id);
        List<Book> booksByAuthor = new ArrayList<>();
        for (Book book: books) {
            if (book.getPublishingHouse().equals(publishingHouse)) {
                booksByAuthor.add(book);
            }
        }
        return booksByAuthor;
    }
    public List<Book> getPublishedAfter(long year) {
        List<Book> booksPublishedAfter = new ArrayList<>();
        for (Book book: books) {
            if (book.getYear() > year) {
                booksPublishedAfter.add(book);
            }
        }
        return booksPublishedAfter;
    }

    @Override
    public String toString() {
        return books.toString();
    }
}
