package com.brom.books.menu;

public interface MenuCommand {
    void execute();
}
