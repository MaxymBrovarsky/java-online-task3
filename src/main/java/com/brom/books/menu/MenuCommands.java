package com.brom.books.menu;

import com.brom.books.constants.MenuShortcuts;
import com.brom.books.constants.Messages;
import com.brom.books.constants.Regexp;
import com.brom.books.dataholder.Authors;
import com.brom.books.dataholder.Books;
import com.brom.books.dataholder.PublishingHouses;
import java.util.Scanner;

public enum MenuCommands implements MenuCommand {
    START(MenuShortcuts.START){
        @Override
        public void execute() {
            PublishingHouses.initialize();
            Authors.initialize();
            Books.initialize();
        }
    },
    QUIT(MenuShortcuts.QUIT) {
        @Override
        public void execute() {
            System.exit(0);
        }
    },
    BOOKSLIST(MenuShortcuts.BOOKS_LIST) {
        @Override
        public void execute() {
            Books books = Books.getInstance();
            System.out.println(books);
        }
    },
    PUBLISHING_HOUSES_LIST(MenuShortcuts.PUBLISHING_HOUSES_LIST) {
        @Override
        public void execute() {
            PublishingHouses publishingHouses = PublishingHouses.getInstance();
            System.out.println(publishingHouses);
        }
    },
    BOOKS_BY_AUTHOR(MenuShortcuts.BOOKS_BY_AUTHOR) {
        @Override
        public void execute() {
            System.out.println(Messages.ENTER_AUTHOR_ID);
            long id = 0;
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNext(Regexp.ID)) {
                id = scanner.nextLong();
            }
            Books books = Books.getInstance();
            System.out.println(books.getByAuthor(id));

        }
    },
    BOOKS_BY_PUBLISHING_HOUSE(MenuShortcuts.BOOKS_BY_PUBLISHING_HOUSE) {
        @Override
        public void execute() {
            System.out.println(Messages.ENTER_PUBLISHING_HOUSE_ID);
            long id = 0;
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNext(Regexp.ID)) {
                id = scanner.nextLong();
            }
            Books books = Books.getInstance();
            System.out.println(books.getByPublishingHouse(id));
        }
    },
    BOOKS_AFTER_YEAR(MenuShortcuts.BOOKS_AFTER_YEAR) {
        @Override
        public void execute() {
            System.out.println(Messages.ENTER_PUBLISHING_YEAR);
            long year = 0;
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNext(Regexp.YEAR)) {
                year = scanner.nextLong();
            }
            Books books = Books.getInstance();
            System.out.println(books.getPublishedAfter(year));
        }
    }
    ;
    private String shortcut;
    MenuCommands(String shortcut) {
        this.shortcut = shortcut;
    }
}
