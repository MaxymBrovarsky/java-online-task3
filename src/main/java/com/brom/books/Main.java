package com.brom.books;

import com.brom.books.constants.MenuShortcuts;
import com.brom.books.constants.Messages;
import com.brom.books.constants.Regexp;
import com.brom.books.menu.MenuCommands;

import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Main main = new Main();
        main.start();
        main.handleInput();
    }
    public void start() {
        MenuCommands.START.execute();
        System.out.println(Messages.HELLO);
        System.out.println(Messages.MENU);
    }
    public void handleInput() {
        while (true) {
            if (scanner.hasNext(Regexp.COMMAND_SHORTCUT)) {
                executeMenuCommand(scanner.nextLine());
            } else {
                scanner.nextLine();
            }
        }
    }
    private void executeMenuCommand(String commandShortcut) {
        switch (commandShortcut) {
            case MenuShortcuts.QUIT:
                MenuCommands.QUIT.execute();
                break;
            case MenuShortcuts.PUBLISHING_HOUSES_LIST:
                MenuCommands.PUBLISHING_HOUSES_LIST.execute();
                break;
            case MenuShortcuts.BOOKS_LIST:
                MenuCommands.BOOKSLIST.execute();
                break;
            case MenuShortcuts.BOOKS_BY_AUTHOR:
                MenuCommands.BOOKS_BY_AUTHOR.execute();
                break;
            case MenuShortcuts.BOOKS_BY_PUBLISHING_HOUSE:
                MenuCommands.BOOKS_BY_PUBLISHING_HOUSE.execute();
                break;
            case MenuShortcuts.BOOKS_AFTER_YEAR:
                MenuCommands.BOOKS_AFTER_YEAR.execute();
                break;
        }
    }
}
