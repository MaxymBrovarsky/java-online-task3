package com.brom.books.constants;

public class Regexp {
    public static final String COMMAND_SHORTCUT = "\\w+";
    public static final String ID = "\\d+";
    public static final String YEAR = "\\d{4}";
}
