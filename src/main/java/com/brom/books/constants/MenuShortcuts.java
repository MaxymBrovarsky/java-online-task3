package com.brom.books.constants;

public class MenuShortcuts {
    public static final String START = "s";
    public static final String QUIT = "q";
    public static final String BOOKS_LIST = "bl";
    public static final String PUBLISHING_HOUSES_LIST = "phl";
    public static final String BOOKS_BY_AUTHOR = "bba";
    public static final String BOOKS_BY_PUBLISHING_HOUSE = "bbph";
    public static final String BOOKS_AFTER_YEAR = "bay";
}
