package com.brom.books.constants;

public class Messages {
    public static final String HELLO = "Hello! Please choose option from menu(see below).";
    public static final String MENU = "Menu(press what in parenthesis):\n1.Print list of books(bl)\n" +
            "2.Print books from certain publishing house(bbph)\n" +
            "3.Print books published after some year(bay)\n" +
            "4.Print books written by some author(bba)";
    public static final String ENTER_PUBLISHING_HOUSE_ID = "Enter publishing house id.";
    public static final String ENTER_AUTHOR_ID = "Enter author id.";
    public static final String ENTER_PUBLISHING_YEAR = "Enter book publishing year.";
}
