package com.brom.books.model;

public enum BindingType {
    HARD,
    TAPE,
    SCREW
}
