package com.brom.books.model;

public class PublishingHouse {
    private long id;
    private String name;
    private String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return this.name + "(" + this.city + ", id = " + this.id + ")";
    }

    public PublishingHouse(long id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public PublishingHouse(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
