package com.brom.books.model;

public class Author {
    private long id;
    private String firstName;
    private String secondName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.secondName + "(id = " + this.id + ")";
    }

    public long getId() {
        return id;
    }

    public Author(long id, String firstName, String secondName) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public Author(long id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Author) {
           if (this.getId() == ((Author) obj).getId()) {
               return true;
           }
        }
        return false;
    }
}
