package com.brom.books.model;

import java.util.List;

public class Book {
    private long id;
    private String title;
    private List<Author> authors;
    private PublishingHouse publishingHouse;
    private int year;
    private int amountOfPages;
    private double price;
    private BindingType bindingType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public PublishingHouse getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(PublishingHouse publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getAmountOfPages() {
        return amountOfPages;
    }

    public void setAmountOfPages(int amountOfPages) {
        this.amountOfPages = amountOfPages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public BindingType getBindingType() {
        return bindingType;
    }

    public void setBindingType(BindingType bindingType) {
        this.bindingType = bindingType;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id: ").append(this.id).append(";\n");
        stringBuilder.append("Title: ").append(this.title).append(";\n");
        stringBuilder.append("Author: ").append(this.authors).append(";\n");
        stringBuilder.append("Publishing house: ").append(this.publishingHouse).append(";\n");
        stringBuilder.append("Year: ").append(this.year).append(";\n");
        stringBuilder.append("Amount of pages: ").append(this.amountOfPages).append(";\n");
        stringBuilder.append("Type of binding: ").append(this.bindingType).append(";\n");
        stringBuilder.append("Price: ").append(this.price).append(";");
        return stringBuilder.toString();
    }

    public Book(long id, String title, List<Author> authors, PublishingHouse publishingHouse, int year, int amountOfPages, double price, BindingType bindingType) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.year = year;
        this.amountOfPages = amountOfPages;
        this.price = price;
        this.bindingType = bindingType;
    }

    public Book(long id, String title, List<Author> authors) {
        this.id = id;
        this.title = title;
        this.authors = authors;
    }

    public Book(long id, String title, List<Author> authors, PublishingHouse publishingHouse, int year) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.year = year;
    }
}
